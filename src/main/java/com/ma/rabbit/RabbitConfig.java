package com.ma.rabbit;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

@Configuration
public class RabbitConfig {

    private static final String QUEUE_ENVIOS = "envios-debitos";
    private static final String EXCHANGE_ENVIOS = "envioDebito";
    private static final String TOPIC_ENVIOS = "envio.creado";

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory cf) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(cf);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Declarables bindings() {
        Collection<Declarable> list = new ArrayList<>();

        list.add(new Queue(QUEUE_ENVIOS, true, false, false));
        list.add(new Binding(QUEUE_ENVIOS, QUEUE, EXCHANGE_ENVIOS, TOPIC_ENVIOS, null));
        return new Declarables(list);
    }

    @Bean
    public TopicExchange envioDebitoExchange() {
        return new TopicExchange(EXCHANGE_ENVIOS);
    }

}
