package com.ma.rabbit;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class EnviosListener {

    @RabbitListener(queues = "envios-debitos")
    public void onCrear(EnvioCreadoEvent event) {
        log.info("recibi evento {}" , event.getId());
    }

}
