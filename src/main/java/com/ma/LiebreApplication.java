package com.ma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiebreApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiebreApplication.class, args);
	}

}
