package com.ma.shared;

import com.ma.rabbit.EnvioCreadoEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

@SpringBootTest
@ExtendWith(OutputCaptureExtension.class)
class EnvioCreadoTest extends ContainerTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    void deberiaCrearEnvio(CapturedOutput output) {
        rabbitTemplate.convertAndSend("envioDebito", "envio.creado", new EnvioCreadoEvent("123"));

        await().atMost(10, TimeUnit.SECONDS).until(() -> output.toString().contains("recibi evento 123"));
    }

}
